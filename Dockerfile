FROM rust

ARG BRANCH=master
ARG TAG=none

WORKDIR /build

RUN git clone https://github.com/getzola/zola --branch ${BRANCH} && \
    cd /build/zola && \
    if [ ${TAG} != "none" ]; then git checkout ${TAG}; fi && \
    cargo build --release && \
    cp /build/zola/target/release/zola /usr/bin/zola && \
    cd / && rm -rf build/zola
